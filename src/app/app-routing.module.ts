import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParticalComponent } from './components/partical/partical.component';


const routes: Routes = [
  {path:'', component:ParticalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
