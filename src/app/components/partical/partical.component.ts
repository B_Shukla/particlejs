import { Component, OnInit } from '@angular/core';
declare var particlesJS: any;

@Component({
  selector: 'app-partical',
  templateUrl: './partical.component.html',
  styleUrls: ['./partical.component.scss']
})
export class ParticalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    particlesJS.load('particles-js', 'assets/data/particles.json', console.log('called-particle.js'));
  }

}
